package co.ivgrp.myplugin;

import org.apache.cordova.api.CallbackContext;
import org.apache.cordova.api.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.util.Log;

public class MyPlugin extends CordovaPlugin 
{
	private final static String DB = "NatDebug";

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException 
	{
		// TODO Auto-generated method stub
		if (action.equals("nativeFunc"))
		{
            String message = args.getString(0); 
            this.nativeFunc(message, callbackContext);
            return true;
        }
        return false;
	}
	
	private void nativeFunc(String message, CallbackContext callbackContext)
	{
		Log.d(DB, "Got message from web UI");
		
		//Callback
		callbackContext.success("Success");
		//callbackContext.error("Any error you want to tell");
    }
}

